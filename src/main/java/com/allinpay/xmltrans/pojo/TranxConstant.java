/**
 *项目：xmlInterf
 *通联支付网络有限公司
 * 作者：张广海
 * 日期：Jan 2, 2013
 * 功能说明：交易demo测试参数
 */
package com.allinpay.xmltrans.pojo;
/**
 */
public class TranxConstant {

	/**
	 * XML交易参数
	 */
	public   static String acctName = "郗兴龙";
	public   static String acctNo = "6225882516636351";
	public   static String amount = "100000";//交易金额
	public   static String bankcode = "0105";//银行代码
	
	public   static String cerPath= "config\\20060400000044502.cer";           
	public   static String merchantId = "200290000019728";

	
	//商户号：200604000000445
	public   static String password = "111111";
	//商户证书信息
	public   static String pfxPassword = "111111";
	public   static String pfxPath= "config\\20029000001972804.p12";

	
	public   static String sum = "200000";//交易总金额
	public   static String tel = "13434343434";
	public   static String tltcerPath= "config\\allinpay-pds.cer";
	public   static String userName = "20029000001972804"; 

 
	
	
}
