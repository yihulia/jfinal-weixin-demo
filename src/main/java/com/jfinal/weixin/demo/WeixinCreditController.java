/**
 * Copyright (c) 2015-2016, Javen Zhou  (javenlife@126.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */
package com.jfinal.weixin.demo;

import com.jfinal.weixin.sdk.api.*;
import com.jfinal.weixin.sdk.jfinal.ApiController;
import com.jfinal.weixin.util.WeixinUtil;

public class WeixinCreditController extends ApiController {

	/**
	 * 如果要支持多公众账号，只需要在此返回各个公众号对应的 ApiConfig 对象即可 可以通过在请求 url 中挂参数来动态从数据库中获取 A
	 */
	public ApiConfig getApiConfig() {
		return WeixinUtil.getApiConfig();
	}

	/**
	 * 提交个人信息 搜集客户端提交过来的个人信息入队到Redis队列，区分区分渠道，暂时是定义1：新颜渠道 信息一般为姓名，身份证号，手机号，银行卡（非必填项）
	 */
	public void commitPersonalInfo() {

	}

	/**
	 * 查询个人信息，提交个人的进本信息后，后台会有定时任务进行执行请求 机构是新颜征信,查询条件是是微信唯一识别
	 */
	public void getPersonalCredit() {

		renderJson(getPara(2));

	}

	/**
	 * 发送验证码接口 key值为手机号，存放在redi中60秒
	 */
	public void sendVerifyCode() {

		renderJson(getPara(2));

	}

}
